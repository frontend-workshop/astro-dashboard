export const data = {
          labels: [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July"
          ],
          datasets: [
            {
              label: new Date().getFullYear(),
              backgroundColor: "#4c51bf",
              borderColor: "#4c51bf",
              data: [65, 78, 66, 44, 56, 67, 75],
              fill: false
            },
            {
              label: new Date().getFullYear() - 1,
              fill: false,
              backgroundColor: "#ed64a6",
              borderColor: "#ed64a6",
              data: [40, 68, 86, 74, 56, 60, 87]
            }
          ]
        }

export const options = {
  responsive: true,
  maintainAspectRatio: false
}