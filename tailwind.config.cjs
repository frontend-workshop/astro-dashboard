/** @type {import('tailwindcss').Config} */
module.exports = {
  // corePlugins: {
  //   preflight: false,
  // },
  content: ["./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}",  
  // 'node_modules/preline/dist/*.js'
],
  theme: {
    extend: {
      fontFamily: {
        sans: ['GitLabMono', 'sans-serif'],
        serif: ['GitLabSans', 'serif'],
 },
      colors: {
        primary: {
          DEFAULT: "#0073FF"
        },
        secondary: {
          DEFAULT: "#FAAD12",
        },
        alert: {
          DEFAULT: "#FFE6F2",
        },
      },
      screens: {
        // small: "600px",
        // 'extra-medium': "905px",
        // 'medium': "1240px",
        // 'large': "1440px",
    },
  }
},
  plugins: [
  // require('@tailwindcss/typography'),
  //   require('@tailwindcss/forms')({ strategy: 'base' }),
  // require('preline/plugin'),
],
}