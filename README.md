# Astro Dashboard - Admin

[Astro Dashboard](https://frontend-workshop.gitlab.io/astro-dashboard/) is an admin dashboard using Tailwind CSS built with [Astro](https://astro.build/) and [Vue](https://vuejs.org/) created by Tarik [@wpplumber](https://upwork.com/freelancers/wpplumber).

## Preview

[![Astro Dashboard - Admin](https://gitlab.com/frontend-workshop/astro-dashboard/-/raw/main/src/assets/img/MBP-Thumbnail.png)](https://frontend-workshop.gitlab.io/astro-dashboard/)

## Bugs and Issues

Have a bug or an issue with this template? [Open a new issue](https://gitlab.com/frontend-workshop/astro-dashboard/-/issues) here on GitLab.

## Copyright and License

Copyright © 2023 Astro Dashboard - Admin by Tarik @wpplumber,[MIT](https://gitlab.com/frontend-workshop/astro-dashboard/-/raw/main/LICENSE.md?ref_type=heads) license.
